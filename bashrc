# whatup

shopt -s globstar # enables ** wildcard for deep searching

# find the real location of this file, like maybe it's symlinked to ~
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

export DEVELOPMENT=1
export MONGO_URL=mongodb://localhost:27017

# not under version control
source "${HOME}/.api-keys.bash"

alias ls='ls --color=auto'
alias ll='ls -l'
alias la='ls -a'
alias lt='ls -tr'
alias l='ls'
alias up='cd ..'
alias xo='xdg-open'
alias stun='systemctl suspend'

# set a graphical sudo password thingy for in-script usage
if command -v ssh-askpass >/dev/null 2>&1 ; then
  export SUDO_ASKPASS=$(which ssh-askpass)
fi

# Git stuff
source "$DIR"/git-completion.bash
alias gs='git status -sb'
alias gd='git diff'
alias gds='git diff --staged'
alias glog='git log --max-count 10 --graph --date=relative --pretty=format:"%Cred%h%Creset %an: %s - %Creset %C(yellow)%d%Creset %Cgreen(%cr)%Creset"'
alias gca="git commit --amend --no-edit"
alias gls='git ls-files -z | xargs -0 -n1 -I{} -- git log -1 --format="%ai {}" {} | sort -r'

function gc {
  git commit -m "$*"
}

# this is my favorite thing
function :w() {
  git add -A
  git add -u

  if [ "$#" != "0" ] ; then
    # new commit with the given message
    git commit -m "$*"
  elif [ "$(git for-each-ref --format='%(upstream:short)' $(git symbolic-ref -q HEAD) | grep '\/')" == '' ]; then
    # no remote
    git commit --amend --no-edit
  elif [ "$(git cherry)" != '' ]; then
    # no pushed
    git commit --amend --no-edit
  else
    # has to be a new commit, prompt for message
    git commit
  fi
}

function pull() {
  # this function needs work
  if [ -z "$(git diff --name-only | head -1)" ]; then
    git pull $@
  else
    git stash >/dev/null 2>&1
    git pull $@
    git stash pop -q
  fi
  git status
}

function push() {
  # also needs work whatever i never use this
  git push $@
}

# ok this is really my favorite thing
alias wow='gs'
alias such=git
alias very=git
  

# git prompt stuff

# Color variables
GREEN="\[\e[1;32m\]"
CYAN="\[\e[1;36m\]"
MAGENTA="\[\e[1;35m\]"
RED="\[\e[1;91m\]"
ENDCOLOR="\[\e[m\]"         # ends the last color

function __git_branch() {
  local branch
  if branch=$(git rev-parse --abbrev-ref HEAD 2>/dev/null); then
    if [[ "$branch" == "HEAD" ]]; then
      branch='detached*'
    fi
    echo " $branch"
  fi
}

export PS1="${GREEN}\u@\h ${CYAN}\w${MAGENTA}\$(__git_branch) \n${RED}♥ ${ENDCOLOR}"

# default programs
#export CC=$(which clang)
#export CXX=$(which clang++)
export CC=$(which gcc)
export CXX=$(which g++)

export VISUAL=vim
export EDITOR="$VISUAL"

# n for nodejs
export N_PREFIX="$DIR/n";

#
# PATH
#
[[ :$PATH: == *":$N_PREFIX/bin:"* ]] || PATH="$N_PREFIX/bin:$PATH"

# workstation-setup/bin
[[ :$PATH: == *":$DIR/bin:"* ]] || PATH="$DIR/bin:$PATH"

# golang
[[ :$PATH: == *":/usr/local/go/bin:"* ]] || PATH="/usr/local/go/bin:$PATH"

# cargo for rust
[[ :$PATH: == *":$HOME/.cargo/bin:"* ]] || PATH="$HOME/.cargo/bin:$PATH"

# meteor for meteor
[[ :$PATH: == *":$HOME/.meteor:"* ]] || PATH="$HOME/.meteor:$PATH"

# miniconda
# https://docs.conda.io/en/latest/miniconda.html
[[ :$PATH: == *":$HOME/miniconda3/bin:"* ]] || PATH="$HOME/miniconda3/bin:$PATH"

# such local bin
[[ :$PATH: == *":$HOME/local/bin:*" ]] || PATH="$HOME/local/bin:$PATH"

# very hq bin
[[ :$PATH: == *":$HOME/hq/bin:*" ]] || PATH="$HOME/hq/bin:$PATH"

# neovim
[[ :$PATH: == *":/opt/nvim-linux64/bin:*" ]] || PATH="/opt/nvim-linux64/bin:$PATH"

# settings for when on windows subsystem for linux
if [[ -e /mnt/d/Users/pbrandt ]] ; then
  export LS_COLORS="ow=01;36;40"
fi


# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/pete/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/pete/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/pete/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/pete/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

. "$HOME/.cargo/env"

# Wine
export WINEARCH=win32
