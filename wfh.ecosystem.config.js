module.exports = {
  apps: [{
    name: 'copernicus',
    script: 'bin/copernicus.sh',
    interpreter: 'bash'
  }]
}

