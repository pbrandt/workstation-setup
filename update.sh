#!/usr/bin/env bash

# exit script if anything goes wrong at all
set -e

# print a nice green ok if something went well
function huzzah() {
  echo -e "[\e[32m ok\e[0m ] $@"
}

function boo() {
  echo -e "[\e[31m bad\e[0m ] $@"
}

# this should not be run as root, if so exit
if [ ${EUID} = 0 ] ; then
  echo do not run as root or with sudo, just as your user. the script will use sudo when it needs permission.
  exit 1
else
  huzzah running as user $USER
fi


DIR="$(cd $( dirname ${BASH_SOURCE[0]}) && pwd)"
cd "$DIR"

# make a bin dir
mkdir -p "$DIR/bin"

# make an autostart dir
mkdir -p $HOME/.config/autostart-scripts

# link config files
echo "linking configuration files"
ln -sfT "$DIR/bashrc" "$HOME/.bashrc"
ln -sfT "$DIR/vimrc" "$HOME/.vimrc"
ln -sfT "$DIR/inputrc" "$HOME/.inputrc"
ln -sfT "$DIR/tmux.conf" "$HOME/.tmux.conf"
ln -sfT "$DIR/condarc" "$HOME/.condarc"
ln -sfT "$DIR/startup.sh" "$HOME/.config/autostart-scripts/startup.sh"
ln -sfT "$DIR/nvim" "$HOME/.config/nvim"

# link directories
echo "linking directories"
ln -nsf "$DIR/vim" "$HOME/.vim"
rsync -av "$DIR"/config/* "$HOME/.config"
mkdir -p "$HOME/.local/share/fonts"
rsync -av "$DIR"/fonts/* "$HOME/.local/share/fonts"

# update fonts
echo "updating font cache..."
fc-cache -f -v > /dev/null

#
# install software
#
sudo apt update
sudo apt-get upgrade

# software development
sudo apt install -y build-essential git vim curl clang cmake ninja-build pkg-config libgtk-3-dev gcc-arm-none-eabi liblapack-dev

# language for typing
sudo apt install -y fcitx fcitx-hangul fcitx-googlepinyin fcitx-m17n

# small programs
sudo apt install -y terminator openvpn octave yakuake zenity

# a/v and creative programs
sudo apt install -y blender gimp inkscape dia vlc

# laptop stuff
sudo apt install -y xbacklight

# absolutely dumb shit
sudo apt install -y libttspico-utils

# programs to install manually
if command -v opera ; then
  huzzah installed opera
else
  boo please install opera from "https://www.opera.com"
fi

if command -v code ; then
  huzzah vs code installed
else
  boo please install vscode from "https://code.visualstudio.com/download"
fi

if command -v conda ; then
  huzzah installed conda
else
  boo please install miniconda from "https://docs.conda.io/en/latest/miniconda.html"
fi

#
# system tweaks
#

# adding to the dialout group allows you to connect to arduinos etc
sudo gpasswd --add $USER dialout

# docker because almost inevitable
sudo gpasswd --add $USER docker

# increasing watch limit
if grep fs.inotify.max_user_watches /etc/sysctl.conf ; then
  huzzah configured watch limit
else
  echo "fs.inotify.max_user_watches=524288" | sudo tee -a /etc/sysctl.conf
fi

# make sure an ssh key is installed
if [ -f ${HOME}/.ssh/id_rsa.pub ] ; then
  huzzah confirmed ssh key exists
else
  ssh-keygen
  huzzah generated ssh key
fi

# git configurations
git config --global user.email "peter.m.brandt@gmail.com"
git config --global user.name "Pete Brandt"
git config --global pull.rebase true


# helper to install vim plugins from the net
function clone_or_pull() {
  short_name=$(echo $@ | sed 's/\.git//g' | sed 's/.*\///g')

  if [ -d "$DIR/vim/bundle/$short_name" ]; then
    cd "$DIR/vim/bundle/$short_name"
    git pull --rebase
    cd -
  else
    cd "$DIR/vim/bundle"
    git clone "$@"
    cd -
  fi
}

echo "installing/updating vim plugins"
clone_or_pull https://github.com/ervandew/supertab
clone_or_pull https://github.com/kien/ctrlp.vim
# clone_or_pull --depth=1 https://github.com/scrooloose/syntastic.git
clone_or_pull https://github.com/tpope/vim-sensible
clone_or_pull https://github.com/tpope/vim-fugitive
clone_or_pull https://github.com/scrooloose/nerdtree
clone_or_pull https://github.com/preservim/nerdcommenter.git


#
# hive stuff
#
if [ -d "$DIR"/n ]; then
  huzzah n installed
else
  echo installing n
  git clone https://github.com/tj/n.git
  ./n/bin/n latest
fi

if [ -f "$DIR"/n/bin/pm2 ]; then
  huzzah pm2 installed
else
  echo installing pm2
  ./n/bin/npm i -g pm2
fi

#
# micro editor (in addition to vim)
#
if [ -f "$DIR"/bin/micro ]; then
  huzzah micro installed
else
  echo installing micro
  curl https://getmic.ro | bash
  mv micro "$DIR"/bin/.
fi

