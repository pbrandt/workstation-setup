# workstation-setup

bashrc etc

run `update.sh` to update your stuff. but be warned that this will probably overwrite your own bashrc and maybe some other configuration files, so i'd read through update.sh first.

# fonts

fonts are rsynced to ~/.local/share/fonts

* Windows fonts like Calibri that don't come with linux
* DIN fonts for technical and industrial applications (pete's favorite font)
* Futura fonts for recapturing the Apollo look and feel
* JetBrains ligature fonts for programming, kind of meh
* Public Sans, a government-built font for readability
* Merriweather, was once a nasa-recommended font for web text
* Source Sans Pro, it's better than Helvetica
* Helvetica, because reasons
* IBM Letter Gothic, you'll see this in old printouts and stuff

To use JetBrains Mono or something else in VS Code, you can edit your font settings to be this:

```
'JetBrains Mono', 'Droid Sans Mono', 'monospace', monospace, 'Droid Sans Fallback'
```

and then check the Enable Font Ligatures box.

LibreOffice's kerning is broken, so fonts look like total shit. If you must do docx or pptx, go find a Windows computer. Honestly if you're writing text on Linux you should probably be doing LaTeX with XeTeX, or Markdown with KaTeX, and version-controlling it and rendering PDFs on occasion. Avoid LibreOffice at all costs.

So here's what i do to make a good design: literally try to recreate the look and feel of something that already exists that i like (rather than pulling a design out the air). Look at theirs and yours side by side and don't stop until they feel the same. I'm an engineer not a designer yo.

