#!/usr/bin/env bash

# this script gets symlinked to ~/.config/autostart-scripts, so i need to find the original dir
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve symlinks
  DIR="$(cd -P "$(dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$(cd -P "$(dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

# start terminator in split view
cd "$DIR"/..
terminator -l dual &
cd "$DIR"

# start the web browser
opera &

# start the drop-down terminal
yakuake &

# start copernicus and whatever else
pm2 start "$DIR"/wfh.ecosystem.config.js

